﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccessShader : MonoBehaviour
{

	[SerializeField] private SkinnedMeshRenderer player;
	private float positionValue = 0;
	
	void Start ()
	{
		print(player.material.GetFloat("Vector1_8BE42E95"));
	}
	
	
	void Update ()
	{
		positionValue += 0.001f;
		player.material.SetFloat("Vector1_8BE42E95", positionValue);
	}
}
